ARG L4TBASE_VERSION="r32.4.3"

FROM nvcr.io/nvidia/l4t-base:${L4TBASE_VERSION}

RUN apt-get update && \
    apt-get install -y \
    python3-pip 

RUN pip3 install docker
RUN mkdir /hoster
WORKDIR /hoster
ADD hoster.py /hoster/

CMD ["python3", "-u", "hoster.py"]




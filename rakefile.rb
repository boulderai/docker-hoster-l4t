RAKEFILE_DIR = File.expand_path(File.dirname(__FILE__))
DOCKER_IMAGE = "gcr.io/external-bai/docker-hoster-l4t"
L4TBASE_VERSION="r32.4.3"
VERSION_FILE = File.join(RAKEFILE_DIR, ".version")

def image_version
    if File.file?(VERSION_FILE)
        return File.read(VERSION_FILE).split[0]
    end
    return "dev-local"
end

desc "Set the version/tag of the images built by this rakefile eg rake set_version[0.0.1-dev]"
task :set_version, [:new_version] do |t, args|
    File.write(VERSION_FILE, args[:new_version])
end

desc "Build the #{DOCKER_IMAGE}:#{image_version} docker image"
task :image do
  sh "docker build -t #{DOCKER_IMAGE}:#{image_version} #{RAKEFILE_DIR}"
end

desc "Build and push the multiplatform #{DOCKER_IMAGE}:#{image_version} docker release"
task :release do
  sh "export DOCKER_CLI_EXPERIMENTAL=enabled && docker buildx create --use && docker buildx build --push --platform linux/arm64/v8 --platform linux/amd64 -t #{DOCKER_IMAGE}:#{L4TBASE_VERSION}-#{image_version} #{RAKEFILE_DIR}"
end

desc "Create a tarball of the the #{DOCKER_IMAGE}:#{image_version} docker image at #{DOCKER_IMAGE}-#{image_version}.tar.gz"
task :tarball => [:image] do
  sh "docker save #{DOCKER_IMAGE}:#{image_version} | gzip > #{DOCKER_IMAGE}-#{image_version}.tar.gz"
end

desc "Create a tarball of the the #{DOCKER_IMAGE}:#{image_version} docker release image at #{DOCKER_IMAGE}-#{image_version}.tar.gz"
task :release_tarball => [:release] do
  sh "docker save #{DOCKER_IMAGE}:#{image_version} | gzip > #{DOCKER_IMAGE}-#{image_version}.tar.gz"
end

